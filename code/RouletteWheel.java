import java.util.*;

public class RouletteWheel 
{
    private Random rand;
    private int spinNumber = 0;

    public RouletteWheel()
    {
        this.rand = new Random();
    }
    
    public void spin()
    {
        this.spinNumber = this.rand.nextInt(37);
    }

    public int getValue()
    {
        return this.spinNumber;
    }
}
