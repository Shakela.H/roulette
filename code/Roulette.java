import java.util.*;
//it could need some more helper methods
public class Roulette {
    public static void main(String[] args) {
        RouletteWheel wheel = new RouletteWheel();
        int yourWallet = 1000;
        Scanner sc = new Scanner(System.in);
        boolean newGame = true;

        while (newGame) {
            if(yourWallet == 0){
                System.out.println("Sorry, you don't have no more money. Come back next time!");
                newGame = false;
                break;
            }
            System.out.println("Would you like to place a bet? If so, enter Y, else any key");
            String answer = sc.next();

            if (answer.equalsIgnoreCase("Y")) {
                boolean betNumberIsValid = false;
                int bet = 0;
                while (!betNumberIsValid) {
                    System.out.println("What number would you like to bet? Choose between 1 and 36");
                    bet = sc.nextInt();

                    if (bet > 0 && bet <= 36) {
                        betNumberIsValid = true;
                    } else {
                        System.out.println("Please enter a valid bet between 1 and 36");
                    }
                }

                boolean betValueIsValid = false;

                while (!betValueIsValid) {
                    System.out.println("How much would you like to bet?");
                    int moneyBetted = sc.nextInt();

                    if (moneyBetted <= yourWallet && moneyBetted > 0) {
                        yourWallet -= moneyBetted;
                        betValueIsValid = true;
                    } else {
                        System.out.println("Please enter a valid bet");
                    }
                }

                wheel.spin();
                int valueOfWheel = wheel.getValue();
                System.out.println("Wheel value: " + valueOfWheel);
                if (valueOfWheel == bet){
                    System.out.println("Congratulations! you won " + betCalculator(bet) + "$!" );
                    yourWallet += betCalculator(bet);
                }
                else{
                    System.out.println("Sorry, you lost.");
                }
                System.out.println("Remaining wallet balance: " + yourWallet);
            } else {
                System.out.println("Thank you for playing!");
                newGame = false;
            }
        }
    }

    private static int betCalculator(int bet){
        int calculatedValue = bet*35;
        return calculatedValue;
    }
}